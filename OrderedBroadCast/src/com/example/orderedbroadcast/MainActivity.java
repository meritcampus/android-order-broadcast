package com.example.orderedbroadcast;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity {
	private static String Log_Tag = "MainActivity";
	 private static String action = "com.example.orderedbroadcast";
	    private static String extras = "Breadcrumb";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		IntentFilter filter=new IntentFilter(action);
		registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle results = getResultExtras(true);
                String trail = results.getString(extras);
                results.putString(extras, trail + "->" + Log_Tag);
                Log.i(Log_Tag, "Main Same Class Receiver");
            }
            }, filter);
		Intent intent = new Intent(action);
	    sendOrderedBroadcast(intent, null, new BroadcastReceiver() {
	        @SuppressLint("NewApi")
	@Override
	        public void onReceive(Context context, Intent intent) {
	            Bundle results = getResultExtras(true);
	            Log.i(Log_Tag, "Main Final Result Receiver = " + results.getString(extras, "nil"));
	        }
	    }, null, Activity.RESULT_OK, null, null);
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
	}
	
	
}
