package com.example.orderedbroadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class ReceiverThree extends BroadcastReceiver {
	 private static String Log_Tag = "ReceiverThree";
	 private static String Extras = "Breadcrumb";
	 
	 @Override
	 public void onReceive(Context context, Intent intent) {
		 Bundle results = getResultExtras(true);
		 results.putString(Extras, Log_Tag);
	 Log.i(Log_Tag, "Priority = 3");
	 }

}
