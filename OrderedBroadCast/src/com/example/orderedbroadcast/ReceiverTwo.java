package com.example.orderedbroadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class ReceiverTwo extends BroadcastReceiver {
	 private static String Log_Tag = "ReceiverTwo";
	 private static String Extras = "Breadcrumb";
	 @Override
	 public void onReceive(Context context, Intent intent) {
	 Bundle results = getResultExtras(true);
	 String trail = results.getString(Extras);
	 results.putString(Extras, trail + "->" + Log_Tag);
	 Log.i(Log_Tag, "Priority = 2");
	 //abortBroadcast();
	 }
	 
	

}
